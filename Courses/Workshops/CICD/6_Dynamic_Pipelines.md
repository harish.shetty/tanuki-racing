# Introduction

In this challenge will walk you through how to set up a dynamic pipeline

# Step 01 - Creating the pipeline script

1. Next the security team has also asked if we can run different scans depending on the results of our jobs. Normally we can use rules, but in this case they also want to reference the results of a script so we are going to demo this to them using a basic dynamic pipeline.

2. Navigate back to the main view of our project by clicking on our project name in the top left of the left hand navigation menu. Once here click through **Edit>Web IDE**

3. Once in the Web IDE we will then want to right click in the file view at the top level and select **New File...**

4. We then will name our file ***pipeline_yaml_creation.sh*** and add the code below to the new file:

    ```
    script1=$(cat << 'EOF'
    image: docker:latest

    include:
    - template: Code-Quality.gitlab-ci.yml
    - template: Jobs/Dependency-Scanning.gitlab-ci.yml
    - template: Jobs/SAST.gitlab-ci.yml
    - template: Jobs/Secret-Detection.gitlab-ci.yml
    EOF
    )

    echo "$script1" > security-config.yml
    ```
5. We then will use the menu on the left hand side to click **Source Control**, add a quick commit message then click **Commit to 'main'**.

6. Once we have that done we can click the popup to go back to our project and start editing our pipeline config.

# Step 02 - Creating the pipeline script

1. Now that we are back in our project we want to actually add the yaml to use our dynamic pipeline code. We will use the left hand navigation menu to click through **Build>Pipeline editor**. 

2. First things first we will want to delete the code below that called our downstream pipeline as we are moving that functionality to our dynamic pipeline:

    ```
    downstream_security:
     stage: extra-security
     trigger:
       include:
             - local: security-pipeline/security.gitlab-ci.yml

    ```

   Remove the "extra-security" stage.

3. Next we will add the following code below to call our script and create the new security job:

   ```
   dynamic-security-build-job:
    image: bash:4.1
    stage: build
    script:
        - chmod +x ./pipeline_yaml_creation.sh
        - ./pipeline_yaml_creation.sh
    artifacts:
        paths:
            - security-config.yml


   dynamic-security-run:
    stage: dynamic
    trigger:
        include:
            - artifact: security-config.yml
              job: dynamic-security-build-job
        strategy: depend
   ```

   Add a stage called "dynamic".

4. Lets go ahead and click **Commit Changes** and use the left hand menu to click through **Build \> Pipelines**, then click the hyperlink from the most recently kicked off pipeline that starts with **<span dir="">_#_</span>**. In the pipeline view as the jobs run click into each of them to see how our added **_dynamic-security-pipeline_** have changed the output.

> If you run into any issues you can use the left hand navigation menu to click through **Build \> Pipelines**, click **Run pipeline**, select **_dynamic-security-pipeline_** and click **Run pipeline** once again.